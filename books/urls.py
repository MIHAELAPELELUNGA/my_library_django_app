from django.contrib import admin
from django.urls import path

from books import views
from books.views import CategoryCreateView, CategoryListView, CategoryUpdateView, CategoryDeleteView, \
    CategoryDetailView, HomeTemplateView, PromotionCreateView, \
    PromotionListView, PromotionUpdateView, PromotionDeleteView, PromotionDetailView, BookCreateView, BookListView, \
    BookUpdateView, BookDeleteView, BookDetailView, AuthorCreateView, AuthorListView, AuthorUpdateView, \
    AuthorDeleteView, AuthorDetailView, AuthorBookCreateView, AuthorBookListView, AuthorBookDeleteView, \
    AuthorBookDetailView, AuthorBookUpdateView, PublisherCreateView, PublisherListView, PublisherUpdateView, \
    PublisherDeleteView, PublisherDetailView, PublisherBookCreateView, PublisherBookListView, \
    PublisherBookUpdateView, PublisherBookDeleteView, PublisherBookDetailView, AboutTemplateView, CartListView, \
    CartDetailView

app_name = 'books'


urlpatterns = [
    path('', HomeTemplateView.as_view(), name='home_page'),
    path('about/', AboutTemplateView.as_view(), name='about_page'),

    path('category-create/', CategoryCreateView.as_view(), name='category_create'),
    path('category-list/', CategoryListView.as_view(), name='category_list'),
    path('category-update/<int:pk>', CategoryUpdateView.as_view(), name='category_update'),
    path('category-delete/<int:pk>', CategoryDeleteView.as_view(), name='category_delete'),
    path('category-detail/<int:pk>', CategoryDetailView.as_view(), name='category_detail'),

    path('promotion-create/', PromotionCreateView.as_view(), name='promotion_create'),
    path('promotion-list/', PromotionListView.as_view(), name='promotion_list'),
    path('promotion-update/<int:pk>', PromotionUpdateView.as_view(), name='promotion_update'),
    path('promotion-delete/<int:pk>', PromotionDeleteView.as_view(), name='promotion_delete'),
    path('promotion-detail/<int:pk>', PromotionDetailView.as_view(), name='promotion_detail'),

    path('books-create/', BookCreateView.as_view(), name='books_create'),
    path('books-list/', BookListView.as_view(), name='books_list'),
    path('books-update/<int:pk>', BookUpdateView.as_view(), name='books_update'),
    path('books-delete/<int:pk>', BookDeleteView.as_view(), name='books_delete'),
    path('books-detail/<int:pk>', BookDetailView.as_view(), name='books_detail'),

    path('authors-create/', AuthorCreateView.as_view(), name='authors_create'),
    path('authors-list/', AuthorListView.as_view(), name='authors_list'),
    path('authors-update/<int:pk>', AuthorUpdateView.as_view(), name='authors_update'),
    path('authors-delete/<int:pk>', AuthorDeleteView.as_view(), name='authors_delete'),
    path('authors-detail/<int:pk>', AuthorDetailView.as_view(), name='authors_detail'),

    path('authors-books-create/', AuthorBookCreateView.as_view(), name='authors_books_create'),
    path('authors-books-list/', AuthorBookListView.as_view(), name='authors_books_list'),
    path('authors-books-update/<int:pk>', AuthorBookUpdateView.as_view(), name='authors_books_update'),
    path('authors-books-delete/<int:pk>', AuthorBookDeleteView.as_view(), name='authors_books_delete'),
    path('authors-books-detail/<int:pk>', AuthorBookDetailView.as_view(), name='authors_books_detail'),

    path('publishers-create/', PublisherCreateView.as_view(), name='publishers_create'),
    path('publishers-list/', PublisherListView.as_view(), name='publishers_list'),
    path('publishers-update/<int:pk>', PublisherUpdateView.as_view(), name='publishers_update'),
    path('publishers-delete/<int:pk>', PublisherDeleteView.as_view(), name='publishers_delete'),
    path('publishers-detail/<int:pk>', PublisherDetailView.as_view(), name='publishers_detail'),

    path('publishers-books-create/', PublisherBookCreateView.as_view(), name='publishers_books_create'),
    path('publishers-books-list/', PublisherBookListView.as_view(), name='publishers_books_list'),
    path('publishers-books-update/<int:pk>', PublisherBookUpdateView.as_view(), name='publishers_books_update'),
    path('publishers-books-delete/<int:pk>', PublisherBookDeleteView.as_view(), name='publishers_books_delete'),
    path('publishers-books-detail/<int:pk>', PublisherBookDetailView.as_view(), name='publishers_books_detail'),

    path('orders-list/', CartListView.as_view(), name='orders_list'),
    path('orders-detail/<int:pk>', CartDetailView.as_view(), name='orders_detail'),

    path('open_cart/', views.open_cart_view, name="open_cart"),
    path('add_product_to_cart/', views.add_product_to_cart, name="add_product_to_cart"),
    path('checkout/', views.checkout_view, name="checkout"),
]