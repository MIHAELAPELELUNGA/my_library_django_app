import random
import string
from decimal import Decimal

from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum, F
from django.utils import timezone

from .utils import unique_order_id_generator
from django.db.models.signals import pre_save


class Category(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    parent_category = models.ForeignKey('books.Category', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f"{self.name} - {self.description}"


class Promotion(models.Model):
    name = models.CharField(max_length=300, null=True, blank=True)
    period_of_promotion = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return f"{self.name} {self.period_of_promotion}"


class Book(models.Model):
    title = models.CharField(max_length=300)
    isbn = models.CharField(max_length=20)
    genre = models.CharField(max_length=200)
    publication_date = models.DateField(null=True, blank=True)
    pages = models.IntegerField()
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    stock = models.IntegerField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    promotion = models.ForeignKey(Promotion, on_delete=models.CASCADE)
    available_after_stock_empty = models.BooleanField()
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        category_name = self.category.name if self.category is not None else "Nici o categorie"
        promotion_name = self.promotion.name if self.promotion is not None else "Nici o promotie"
        return f"{self.title}"


class Author(models.Model):
    name = models.CharField(max_length=300)
    origin_author = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return f"{self.name}"


class AuthorBook(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.book} {self.author}"


class Publisher(models.Model):
    name = models.CharField(max_length=300)
    location = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return f"{self.name} {self.location}"


class PublisherBook(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.book} {self.publisher}"


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=100)
    date_ordered = models.DateTimeField(default=timezone.now)
    order_id = models.CharField(max_length=120)

    def __str__(self):
        return self.order_id


def pre_save_create_order_id(sender, instance, *args, **kwargs):
    if not instance.order_id:
        instance.order_id = unique_order_id_generator(instance)


pre_save.connect(pre_save_create_order_id, sender=Cart)


class CartItem(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    cantitate = models.IntegerField()
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.book} {self.cantitate} {self.cart}"

    def get_total(self):
        total = self.book.price * self.cantitate
        return total


