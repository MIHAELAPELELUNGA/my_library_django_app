from django.contrib import admin

from books.models import *

admin.site.register(Book)
admin.site.register(Category)
admin.site.register(Promotion)
admin.site.register(Author)
admin.site.register(AuthorBook)
admin.site.register(Publisher)
admin.site.register(PublisherBook)
admin.site.register(Cart)
admin.site.register(CartItem)
