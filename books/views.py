import random
import string

from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import loader
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView, TemplateView

from books.models import Category, Book, Promotion, Author, AuthorBook, Publisher, PublisherBook, Cart, CartItem
from users.models import ExtendUser


def populate_common_context(context, request):
    context['categories'] = Category.objects.all()
    context['current_user'] = request.user
    return context


class BaseView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(BaseView, self).get_context_data()
        context = populate_common_context(context, self.request)
        return context


class BaseDetailView(DetailView):
    def get_context_data(self, **kwargs):
        context = super(BaseDetailView, self).get_context_data()
        context = populate_common_context(context, self.request)
        return context


class BaseListView(ListView):
    def get_context_data(self, **kwargs):
        context = super(BaseListView, self).get_context_data()
        context = populate_common_context(context, self.request)
        return context


class BaseCreateView(CreateView):
    def get_context_data(self, **kwargs):
        context = super(BaseCreateView, self).get_context_data()
        context = populate_common_context(context, self.request)
        return context


class BaseUpdateView(UpdateView):
    def get_context_data(self, **kwargs):
        context = super(BaseUpdateView, self).get_context_data()
        context = populate_common_context(context, self.request)
        return context


class BaseDeleteView(DeleteView):
    def get_context_data(self, **kwargs):
        context = super(BaseDeleteView, self).get_context_data()
        context = populate_common_context(context, self.request)
        return context


class HomeTemplateView(BaseView):
    template_name = 'home.html'


class AboutTemplateView(BaseView):
    template_name = 'about.html'


class CategoryCreateView(LoginRequiredMixin, BaseCreateView):
    model = Category
    template_name = 'category/category_create.html'
    fields = '__all__'
    success_url = reverse_lazy('books:category_list')


class CategoryListView(LoginRequiredMixin, BaseListView):
    template_name = 'category/category_list.html'
    model = Category
    context_object_name = 'all_categories'


class CategoryUpdateView(LoginRequiredMixin, BaseUpdateView):
    template_name = 'category/category_update.html'
    model = Category
    fields = '__all__'
    success_url = reverse_lazy('books:category_list')


class CategoryDeleteView(LoginRequiredMixin, BaseDeleteView):
    template_name = 'category/category_delete.html'
    model = Category
    success_url = reverse_lazy('books:category_list')


class CategoryDetailView(LoginRequiredMixin, BaseDetailView):
    model = Category
    template_name = 'category/category_detail.html'
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data()
        context['books'] = Book.objects.filter(category_id=self.object.pk)
        context['authors_books'] = AuthorBook.objects.filter(book__in=context['books'])
        context['publishers_books'] = PublisherBook.objects.filter(book__in=context['books'])
        return context


class PromotionCreateView(LoginRequiredMixin, BaseCreateView):
    model = Promotion
    template_name = 'promotion/promotion_create.html'
    fields = '__all__'
    success_url = reverse_lazy('books:promotion_list')


class PromotionListView(LoginRequiredMixin, BaseListView):
    template_name = 'promotion/promotion_list.html'
    model = Promotion
    context_object_name = 'all_promotions'


class PromotionUpdateView(LoginRequiredMixin, BaseUpdateView):
    template_name = 'promotion/promotion_update.html'
    model = Promotion
    fields = '__all__'
    success_url = reverse_lazy('books:promotion_list')


class PromotionDeleteView(LoginRequiredMixin, BaseDeleteView):
    template_name = 'promotion/promotion_delete.html'
    model = Promotion
    success_url = reverse_lazy('books:promotion_list')


class PromotionDetailView(LoginRequiredMixin, BaseDetailView):
    model = Promotion
    template_name = 'promotion/promotion_detail.html'
    context_object_name = 'promotion'

    def get_context_data(self, **kwargs):
        context = super(PromotionDetailView, self).get_context_data()
        context['books'] = Book.objects.filter(promotion_id=self.object.pk)
        context['authors_books'] = AuthorBook.objects.filter(book__in=context['books'])
        context['publishers_books'] = PublisherBook.objects.filter(book__in=context['books'])
        return context


class BookCreateView(LoginRequiredMixin, BaseCreateView):
    model = Book
    template_name = 'books/books_create.html'
    fields = '__all__'
    success_url = reverse_lazy('books:books_list')
    queryset = Book.objects.all()

    def get_context_data(self, **kwargs):
        context = super(BookCreateView, self).get_context_data()
        context['authors'] = Author.objects.all()
        context['publishers'] = Publisher.objects.all()
        context['books'] = self.queryset
        return context

    def post(self, request, *args, **kwargs):
        response = super(BookCreateView, self).post(request)
        author_id = request.POST.get('author_id')
        publisher_id = request.POST.get('publisher_id')
        AuthorBook.objects.create(author_id=author_id, book_id=self.object.id)
        PublisherBook.objects.create(publisher_id=publisher_id, book_id=self.object.id)
        return response


class BookListView(LoginRequiredMixin, BaseListView):
    model = Book
    template_name = 'books/books_list.html'
    context_object_name = 'books'
    queryset = Book.objects.all()

    def get_context_data(self, **kwargs):
        context = super(BookListView, self).get_context_data()
        context['authors'] = Author.objects.all()
        context['authors_books'] = AuthorBook.objects.all()
        return context


class BookUpdateView(LoginRequiredMixin, BaseUpdateView):
    template_name = 'books/books_update.html'
    model = Book
    fields = '__all__'
    success_url = reverse_lazy('books:books_list')


class BookDeleteView(LoginRequiredMixin, BaseDeleteView):
    template_name = 'books/books_delete.html'
    model = Book
    success_url = reverse_lazy('books:books_list')


class BookDetailView(LoginRequiredMixin, BaseDetailView):
    model = Book
    template_name = 'books/books_detail.html'
    context_object_name = 'book'

    def get_context_data(self, **kwargs):
        context = super(BookDetailView, self).get_context_data()
        context['books'] = Book.objects.filter()
        context['authors_books'] = AuthorBook.objects.filter(book__in=context['books'])
        context['publishers_books'] = PublisherBook.objects.filter(book__in=context['books'])
        return context


class AuthorCreateView(LoginRequiredMixin, BaseCreateView):
    model = Author
    template_name = 'authors/authors_create.html'
    fields = '__all__'
    success_url = reverse_lazy('books:authors_list')


class AuthorListView(LoginRequiredMixin, BaseListView):
    template_name = 'authors/authors_list.html'
    model = Author
    context_object_name = 'all_authors'


class AuthorUpdateView(LoginRequiredMixin, BaseUpdateView):
    template_name = 'authors/authors_update.html'
    model = Author
    fields = '__all__'
    success_url = reverse_lazy('books:authors_list')


class AuthorDeleteView(LoginRequiredMixin, BaseDeleteView):
    template_name = 'authors/authors_delete.html'
    model = Author
    success_url = reverse_lazy('books:authors_list')


class AuthorDetailView(LoginRequiredMixin, BaseDetailView):
    model = Author
    template_name = 'authors/authors_detail.html'
    context_object_name = 'author'


class AuthorBookCreateView(LoginRequiredMixin, BaseCreateView):
    model = AuthorBook
    template_name = 'authors/authors_books_create.html'
    fields = '__all__'
    success_url = reverse_lazy('books:authors_books_list')


class AuthorBookListView(LoginRequiredMixin, BaseListView):
    template_name = 'authors/authors_books_list.html'
    model = AuthorBook
    context_object_name = 'all_authors_books'


class AuthorBookUpdateView(LoginRequiredMixin, BaseUpdateView):
    template_name = 'authors/authors_books_update.html'
    model = AuthorBook
    fields = '__all__'
    success_url = reverse_lazy('books:authors_books_list')


class AuthorBookDeleteView(LoginRequiredMixin, BaseDeleteView):
    template_name = 'authors/authors_books_delete.html'
    model = AuthorBook
    success_url = reverse_lazy('books:authors_books_list')


class AuthorBookDetailView(LoginRequiredMixin, BaseDetailView):
    model = AuthorBook
    template_name = 'authors/authors_books_detail.html'
    context_object_name = 'author_book'


class PublisherCreateView(LoginRequiredMixin, BaseCreateView):
    model = Publisher
    template_name = 'publishers/publishers_create.html'
    fields = '__all__'
    success_url = reverse_lazy('books:publishers_list')


class PublisherListView(LoginRequiredMixin, BaseListView):
    template_name = 'publishers/publishers_list.html'
    model = Publisher
    context_object_name = 'all_publishers'


class PublisherUpdateView(LoginRequiredMixin, BaseUpdateView):
    template_name = 'publishers/publishers_update.html'
    model = Publisher
    fields = '__all__'
    success_url = reverse_lazy('books:publishers_list')


class PublisherDeleteView(LoginRequiredMixin, BaseDeleteView):
    template_name = 'publishers/publishers_delete.html'
    model = Publisher
    success_url = reverse_lazy('books:publishers_list')


class PublisherDetailView(LoginRequiredMixin, BaseDetailView):
    model = Publisher
    template_name = 'publishers/publishers_detail.html'
    context_object_name = 'publisher'


class PublisherBookCreateView(LoginRequiredMixin, BaseCreateView):
    model = PublisherBook
    template_name = 'publishers/publishers_books_create.html'
    fields = '__all__'
    success_url = reverse_lazy('books:publishers_books_list')


class PublisherBookListView(LoginRequiredMixin, BaseListView):
    template_name = 'publishers/publishers_books_list.html'
    model = PublisherBook
    context_object_name = 'all_publishers_books'


class PublisherBookUpdateView(LoginRequiredMixin, BaseUpdateView):
    template_name = 'publishers/publishers_books_update.html'
    model = PublisherBook
    fields = '__all__'
    success_url = reverse_lazy('books:publishers_books_list')


class PublisherBookDeleteView(LoginRequiredMixin, BaseDeleteView):
    template_name = 'publishers/publishers_books_delete.html'
    model = PublisherBook
    success_url = reverse_lazy('books:publishers_books_list')


class PublisherBookDetailView(LoginRequiredMixin, BaseDetailView):
    model = PublisherBook
    template_name = 'publishers/publishers_books_detail.html'
    context_object_name = 'publisher_book'


class CartListView(LoginRequiredMixin, BaseListView):
    template_name = 'orders/orders_list.html'
    model = Cart
    context_object_name = 'all_orders'

    def get_context_data(self, **kwargs):
        context = super(CartListView, self).get_context_data()
        context['extended_users'] = ExtendUser.objects.all()
        return context


class CartDetailView(LoginRequiredMixin, BaseDetailView):
    model = Cart
    template_name = 'orders/orders_detail.html'
    context_object_name = 'order'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        extended_users = ExtendUser.objects.filter()
        cart_items = CartItem.objects.filter(cart_id=context['order'].id)
        total, count = calculate_total(context['order'].id)
        context = {'extended_users': extended_users, 'cart_items': cart_items, 'total': total, 'count': count}
        context = populate_common_context(context, self.request)
        return context


@login_required
def open_cart_view(request):
    open_carts = Cart.objects.filter(user=request.user, status='open')
    if len(open_carts) > 0:
        open_cart = open_carts[0]
    else:
        open_cart = Cart.objects.create(user=request.user, status='open')
    cart_items = CartItem.objects.filter(cart=open_cart)
    total, count = calculate_total(open_cart.id)
    context = {'open_cart': open_cart, 'cart_items': cart_items, 'total': total, 'count': count}
    context = populate_common_context(context, request)
    return render(request, 'open_cart.html', context)


@login_required
def add_product_to_cart(request):
    print(request.POST)
    open_carts = Cart.objects.filter(user=request.user, status='open')
    if len(open_carts) > 0:
        open_cart = open_carts[0]
    else:
        open_cart = Cart.objects.create(user=request.user, status='open')
    book_id = request.POST.get('book_id', '')
    cantitate = request.POST.get('cantitate', 1)
    cart_items = CartItem.objects.filter(cart=open_cart, book_id=book_id)
    if len(cart_items) > 0:
        cart_item = cart_items[0]
        cart_item.cantitate += int(cantitate)
        cart_item.save()
    else:
        cart_item = CartItem.objects.create(book_id=book_id, cantitate=cantitate, cart=open_cart)
    if int(cart_item.cantitate) <= 0:
        cart_item.delete()

    return redirect(reverse_lazy('books:open_cart'))


@login_required
def checkout_view(request):
    cart_id = request.POST.get('cart_id', '')
    print('trying to closed' + str(cart_id))
    cart = Cart.objects.get(id=int(cart_id))
    cart_items = CartItem.objects.filter(cart=cart)
    if len(cart_items) > 0:
        cart.status = 'closed'
        cart.save()
        print('cart closed')
    return redirect(reverse_lazy('books:open_cart'))


def calculate_total(cart_id):
    cart_items = CartItem.objects.filter(cart_id=cart_id)
    total = 0
    count = 0
    for cart_item in cart_items:
        total += cart_item.book.price * cart_item.cantitate
        count += cart_item.cantitate
    return total, count


# cod pt primire email la resetarea parolei
def email(request):
    subject = 'Thank you for registering to our site'
    message = ' it  means a world to us '
    email_from = settings.EMAIL_HOST_USER
    recipient_list = ['receiver@gmail.com',]
    send_mail( subject, message, email_from, recipient_list )
    return redirect('redirect to a new page')
