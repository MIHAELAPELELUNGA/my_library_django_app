from django.contrib.auth.models import User, AbstractUser
from django.db import models


class ExtendUser(User):
    phone = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


