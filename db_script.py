# import os
# import django
#
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'my_library_django_app.settings')
# django.setup()
#
# from books.models import Category, Promotion, Book, Author, AuthorBook, Publisher, PublisherBook
#
# ##categoriile
# category1 = Category.objects.create(name="ARTA, ARHITECTURA SI FOTOGRAFIE",
#                                     description="Carti despre arta lumii, arhitectura si fotografie")
#
# category2 = Category.objects.create(name="BELETRISTICA",
#                                     description="Literatura: proza, poezie, narațiuni de călătorii, articole de critică literară și estetică")
#
# category3 = Category.objects.create(name="BIOGRAFII SI MEMORIALISTICA",
#                                     description="Biografii si memorii ale unor personalitati din cultura si istoria lumii")
#
# category4 = Category.objects.create(name="CALCULATOARE/IT",
#                                     description="Carti despre domeniul IT si calculatoare")
#
# category5 = Category.objects.create(name="CARTI IN LIMBI STRAINE",
#                                     description="Carti in limbi straine")
#
# category6 = Category.objects.create(name="CARTI PENTRU COPII",
#                                     description="Carti pentru copii de toate varstele")
#
# category7 = Category.objects.create(name="COMUNICARE",
#                                     description="Carti despre comunicare si relatii publice")
#
# category8 = Category.objects.create(name="DEZVOLTARE PERSONALA",
#                                     description="Carti despre cunoastere si dezvoltare personala")
#
# category9 = Category.objects.create(name="DICTIONARE SI ENCICLOPEDII",
#                                     description="Dictionare si enciclopedii din diverse domenii")
#
# category10 = Category.objects.create(name="DREPT",
#                                     description="Carti despre legislatie si drept")
#
# category11 = Category.objects.create(name="ECONOMIE SI FINANTE",
#                                     description="Carti despre economie si finante")
#
# category12 = Category.objects.create(name="FICTIUNE",
#                                     description="Fascinante carti de fictiune")
#
# category13 = Category.objects.create(name="FILOLOGIE SI FILOSOFIE",
#                                     description="Carti de filologie si filosofie")
#
# category14 = Category.objects.create(name="GASTRONOMIE",
#                                     description="Carti despre cultura gastronomica")
#
# category15 = Category.objects.create(name="HARTI",
#                                     description="Hartile lumii")
#
# category16 = Category.objects.create(name="GHIDURI DE CALATORIE",
#                                     description="Minunate ghiduri de calatorie")
#
# category17 = Category.objects.create(name="INVATA LIMBI STRAINE",
#                                     description="Carti despre invatarea limbilor straine")
#
# category18 = Category.objects.create(name="ISTORIE",
#                                     description="Carti de istorie si civilizatie")
#
# category19 = Category.objects.create(name="PSIHOLOGIE",
#                                     description="Carti de psihologie")
#
# category20 = Category.objects.create(name="RELIGIE SI SPIRITUALITATE",
#                                     description="Carti despre spiritualitate si religie")
#
# category21 = Category.objects.create(name="REVISTE SI ALMANAHURI",
#                                     description="Reviste si almanahuri")
#
# category22 = Category.objects.create(name="SOCIOLOGIE",
#                                     description="Carti de sociologie")
#
# category23 = Category.objects.create(name="STIINTE",
#                                     description="Carti despre stiinte")
#
# category24 = Category.objects.create(name="STIINTE POLITICE",
#                                     description="Carti despre stiinte politice")
#
# promotion1 = Promotion.objects.create(name="Promotie de Paste", period_of_promotion="20.03.2021 - 02.05.2021")
#
# promotion2 = Promotion.objects.create(name="Promotie de Primavara", period_of_promotion="01.03.2021 - 31.05.2021")
#
#
# # aici incepe categoria "ARTA, ARHITECTURA SI FOTOGRAFIE"
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book1 = Book.objects.create(title="100 Mari Artisti. O calatorie vizuala de la Fra Angelico la Andy Warhol",
#                             isbn="978-9736795626", genre="enciclopedie", publication_date="2008-1-1", pages=208,
#                             description="100 Mari Artisti prezinta tot ce este mai valoros in pictura europeana, de la Fra Angelico la Whistler, de la arta secolului al XIII-lea la aceea a epocii moderne. Realizata in conditii grafice de exceptie, lucrarea isi propune sa-l poarte pe cititor intr-o lunga calatorie, pentru a arata in ce mod a fost interpretata traditia europeana in diferite colturi ale pamantului, din Lumea Noua pana in Orientul Indepartat. Fiecare articol cuprinde un comentariu referitor la timpul si locul in care artistul respectiv si-a desfasurat activitatea, totul prezentat in contextul istoric, precum si doua imagini color. Speram ca prezenta lucrare va constitui material bibliografic pentru studenti si artisti, dar si o carte care sa faca placere cititorului care admira pictura si este interesat sa afle ce rol au talentul si stilul personal in procesul de creatie.",
#                             price=48.20, stock=5, category=category1, promotion=promotion1,
#                             available_after_stock_empty=True)
# author1 = Author.objects.create(name="Charlotte Gerlings", origin_author="Reading, Regatul Unit")
# authorbook1 = AuthorBook.objects.create(book=book1, author=author1)
# publisher1 = Publisher.objects.create(name="Editura Aramis", location="Bucuresti - Romania")
# publisherbook1 = PublisherBook.objects.create(book=book1, publisher=publisher1)
#
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book2 = Book.objects.create(title="Arta. O istorie ilustrata. Ghid ilustrat complet",
#                             isbn="978-606-33-3874-8", genre="enciclopedie", publication_date="2020-11-1", pages=512,
#                             description="Fă un tur ghidat al celor mai influente 2 500 de picturi și sculpturi ale lumii, de la arta preistorică la capodoperele modernității. Cu o prezentare atrăgătoare, ca într-un catalog cronologic, Arta înfățișează mai bine de 700 de artiști de-a lungul pietrelor de hotar ale istoriei artei, cu analize amănunțite ale unor opere de căpătâi, pentru a facilita înțelegerea viziunii și a tehnicilor artiștilor.Cuprinzătoare, de autoritate și excelentă vizual, această carte este ghidul complet al simezei din propria ta casă.",
#                             price=149.94, stock=5, category=category1, promotion=promotion1,
#                             available_after_stock_empty=True)
# author2 = Author.objects.create(name="Dorling Kindersley", origin_author="Londra, Regatul Unit")
# authorbook2 = AuthorBook.objects.create(book=book2, author=author2)
# publisher2 = Publisher.objects.create(name="Editura Litera", location="Bucuresti - Romania")
# publisherbook2 = PublisherBook.objects.create(book=book2, publisher=publisher2)
#
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book3 = Book.objects.create(title="Minuni ale lumii create de om",
#                             isbn="978-606-33-6378-8", genre="enciclopedie", publication_date="2020-11-1", pages=336,
#                             description="„Să construiești, să construiești! Aceasta este cea mai nobilă dintre toate artele.“ Henry Wadsworth Longfellow.Descoperă cele mai incredibile statui, monumente, temple, poduri și orase antice, de la Stonehenge la Sagrada Família, cu această carte fără egal care îți prezintă cu text și imagini cele mai faimoase clădiri și structuri create de om.",
#                             price=119.94, stock=4, category=category1, promotion=promotion1,
#                             available_after_stock_empty=True)
# authors = Author.objects.filter(name="Dorling Kindersley", origin_author="Londra, Regatul Unit")
# author2 = authors[0]
# authorbook3 = AuthorBook.objects.create(book=book3, author=author2)
# publishers = Publisher.objects.filter(name="Editura Litera", location="Bucuresti - Romania")
# publisher2 = publishers[0]
# publisherbook3 = PublisherBook.objects.create(book=book3, publisher=publisher2)
#
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book4 = Book.objects.create(title="100 de fotografii care au schimbat lumea",
#                             isbn="978-606-683-516-9", genre="istorie, fotografie si arta", publication_date="2017-01-1", pages=223,
#                             description="O carte-album de colectie cu cele mai importante 100 de fotografii din istoria lumii si descrierea istoriei acestora.Momente marete din istorie imortalizate in 100 de fotografii!De la deschiderea mormantului faraonului Tutankhamon la primul zbor al fratilor Wright, de la ciuperca atomica din Nagasaki la prima aterizare pe luna, cele 100 de fotografii marcheaza momentele cele mai impactante din istoria omenirii. Acestea ne reamintesc nu numai ca fotografia retine evenimentele majore din intreaga lume, dar si ca este un mod de a aduce in atentia  opiniei publice realitatea acestor evenimente. Aceasta carte cu o realizare grafica de exceptie, ce include comentarii pentru fiecare imagine, este pur si simplu de nelipsit din biblioteca ta",
#                             price=141.99, stock=4, category=category1, promotion=promotion1,
#                             available_after_stock_empty=True)
# author3 = Author.objects.create(name="Margherita Giacosa", origin_author="Italia")
# author4 = Author.objects.create(name="Roberto Mottadelli", origin_author="Italia")
# author5 = Author.objects.create(name="Gianni Morelli", origin_author="Italia")
# authorbook4 = AuthorBook.objects.create(book=book4, author=author3)
# authorbook5 = AuthorBook.objects.create(book=book4, author=author4)
# authorbook6 = AuthorBook.objects.create(book=book4, author=author5)
# publisher3 = Publisher.objects.create(name="Didactica Publishing House", location="Bucuresti - Romania")
# publisherbook4 = PublisherBook.objects.create(book=book4, publisher=publisher3)
#
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book5 = Book.objects.create(title="Luvru. Toate picturile",
#                             isbn="978-606-33-4153-3", genre="arta", publication_date="2020-05-1", pages=768,
#                             description="Picturile de la Muzeul Luvru reprezintă cea mai bogată și cea mai vastă colecție de artă europeană. Colecția de artă franceză din secolele XV și XVI este de neegalat. Lucrările geniilor din Renașterea italiană se numără printre cele mai cunoscute din lume. Creațiile pictorilor baroci flamanzi și olandezi redefinesc înțelegerea a ceea ce înseamnă timpurile moderne. Iar în ce îi privește pe pictorii spanioli, tablourile exprimă o forță și o pasiune unice."
#                                         "Fiecare pictură din colecția permanentă a Muzeului Luvru – 3 022 de lucrări în total – se regăsește în acest volum. Picturile sunt organizate și divizate în patru mari colecții: Școala Italiană, Școlile nordice, Școala Franceză și Școala Spaniolă. Fiecare colecție este prezentată cronologic, în funcție de data nașterii pictorului. Printre pictorii din acest volum se numără Giotto, Fra Angelico, Ghirlandaio, Botticelli, Mantegna, Leonardo da Vinci, Rafael, Tițian, Veronese, Caravaggio, Jan van Eyck, Breugel, Memling, Holbein, Antoon van Dyck, Rembrandt, Vermeer, Dürer, Constable, Chardin, Clouet, David, Delacroix, La Tour, Fragonard, Gericault, Ingres, Poussin, Watteau, El Greco, Goya, Murillo, Velázquez, Zurbarán, dar și alte sute de artiști."
#                                         "Fiecare pictură este însoțită de un set de informații care cuprinde titlul lucrării, data, dimensiunile, materialele folosite, locația în cadrul muzeului și numărul de inventar. Peste 400 dintre aceste tablouri dispun de descrieri și comentarii ale istoricilor artei Anja Grebe și Vincent Pomarède. Aceste descrieri îi explică cititorului care sunt detaliile pe care trebuie să le urmărească atunci când privește imaginea. Totodată, ele cuprind informații cu privire la inspirația artiștilor și la impactul pe care aceștia l-au avut asupra istoriei artei.",
#                             price=199.92, stock=5, category=category1, promotion=promotion1,
#                             available_after_stock_empty=True)
# author6 = Author.objects.create(name="Colectiv redactional", origin_author="Franta")
# authorbook7 = AuthorBook.objects.create(book=book5, author=author6)
# publishers = Publisher.objects.filter(name="Editura Litera", location="Bucuresti - Romania")
# publisher2 = publishers[0]
# publisherbook5 = PublisherBook.objects.create(book=book5, publisher=publisher2)
#
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book6 = Book.objects.create(title="Vatican. Toate picturile și frescele vechilor maeștri",
#                             isbn="978-606-33-3123-7", genre="album de arta", publication_date="2018-12-1", pages=528,
#                             description="Toate picturile și frescele vechilor maeștri expuse în galeriile, capelele, bibliotecile și palatele muzeelor de la Vatican, plus alte sute de comori din colecția papală - aproape 1000 de lucrări faimoase - într-un volum uimitor!"
#                                         "Vaticanul este una dintre minunile lumii. Acest bastion istoric și spiritual al creștinismului găzduiește multe muzee și palate, dar și una dintre cele mai bune colecții de artă ale umanității. Lucrările de interes includ frescele Capelei Sixtine și Pietà a lui Michelangelo; frescele lui Rafael; lucrări de Giotto, Fra Angelico, Țițian și Caravaggio și unele dintre cele mai frumoase statui, manuscrise, lucrări de arhitectură și grădini ale lumii, dar și cele mai prețioase artefacte ale creștinătății."
#                                         "Fiecare pictură și frescă expusă la Vatican și aparținând Vechilor Maeștri - pictură europeană de până la 1800 -, plus o selecție de alte peste 300 de lucrări de artă, incluzând pictură modernă, sculptură, tapiserie și alte artefacte."
#                                         "Cartea este organizată în 22 de secțiuni reprezentând muzeele și zonele Vaticanului, inclusiv Pinacoteca, Capela Sixtină, Camerele Rafael, Apartamentele Borgia, Palatele Vaticane și bazilica Sfântul Petru.Toate cele 976 de lucrări de artă sunt complet adnotate cu titlul lor și numele artistului care le-a creat, data realizării, data nașterii și a morții artistului, materalul folosit și locul în care sunt păstrate la Vatican."
#                                         "Cartea include și lucrări din Egiptul antic, din Mesopotamia, Grecia și Roma, dar si multe alte lucrări de artă religioasă și seculară, cuprinzând și o selecție de artă și artefacte din secolul XX."
#                                         "O sută optzeci dintre cele mai cunoscute și semnificative opere de artă sunt prezentate de istoricul de artă Anja Grebe, cu discuții despre atributele-cheie ale capodoperelor."
#                                         "Include trei planșe pentru a admira cele mai cunoscute opere de artă de la Vatican, la dimensiuni mai mari."
#                                         "Având peste 500 de pagini, prezentul volum înfățișează exhaustiv una dintre cele mai rafinate și mai importante colecții de artă ale lumii.",
#                             price=149.94, stock=5, category=category1, promotion=promotion2,
#                             available_after_stock_empty=True)
# author7 = Author.objects.create(name="Colectiv redactional", origin_author="Italia")
# authorbook8 = AuthorBook.objects.create(book=book6, author=author7)
# publishers = Publisher.objects.filter(name="Editura Litera", location="Bucuresti - Romania")
# publisher2 = publishers[0]
# publisherbook6 = PublisherBook.objects.create(book=book6, publisher=publisher2)
#
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book7 = Book.objects.create(title="Arhitectura lumii. Capodoperele",
#                             isbn="978-973-19-8401-8", genre="arta si arhitectura", publication_date="2009-11-1", pages=320,
#                             description="Ce contribuie la măreţia unei clădiri? Iată o întrebare care i-a preocupat de secole în egală măsură pe arhitecţi, critici şi călători. Volumul de faţă surprinde varietatea uimitoare a capodoperelor arhitecturale din lumea întreagă, aparţinând atât culturii occidentale, cât şi celei orientale. Arhitectura lumii. Capodoperele este un studiu accesibil, complet ilustrat, cu fotografii inedite realizate de autor şi detalii speciale care pun în valoare unicitatea fiecărei clădiri."
#                                         "O excepţională enciclopedie de arhitectură, această carte-cadou dezvăluie o lume a frumuseţii şi a ingeniozităţii. Peste 350 de fotografii îl însoţesc pe cititor într-o călătorie grandioasă printre capodoperele arhitecturale aparţinând a peste două milenii de civilizaţie: Hagia Sofia, catedralele gotice şi renascentiste ale Europei, capodoperele islamice din Iran, Fortul Agra şi Taj Mahal, Palatul Westminster din Londra, capela lui Le Corbusier construită pentru Catedrala Notre Dame du Haut din Ronchamp şi celebrul Muzeu Guggenheim din Bilbao proiectat de Ghery."
#                                         "Luând anul 1500 ca reper în structurarea lucrării, Will Pryce prezintă peste 80 de clădiri, dintre care peste 40 sunt analizate prin fotografii detaliate. Textele explicative furnizează informaţii suplimentare despre cultură, civilizaţie şi, implicit, evoluţia istoriei arhitecturii."
#                                         "Pentru cei care apreciază arhitectura, realizările ei uimitoare şi modul în care ne inspiră sau ne tăie respiraţia, această carte captează uimirea şi emoţia resimţite la vederea clădirilor care au marcat istoria arhitecturii. O sursă de fascinaţie şi încântare pentru mulţi ani de-acum înainte."
#                                         "Will Pryce este fotograf profesionist, deţinător a numeroase premii în domeniu. S-a specializat iniţial în arhitectură la Cambridge University şi la Royal College of Art din Londra, urmând apoi cursuri de fotojurnalism la The London College of Printing.",
#                             price=153.36, stock=4, category=category1, promotion=promotion2,
#                             available_after_stock_empty=True)
# author8 = Author.objects.create(name="Will Pryce", origin_author="Londra, Regatul Unit")
# authorbook9 = AuthorBook.objects.create(book=book7, author=author8)
# publisher4 = Publisher.objects.create(name="Editura Vellant", location="Bucuresti - Romania")
# publisherbook7 = PublisherBook.objects.create(book=book7, publisher=publisher4)
#
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book8 = Book.objects.create(title="Architecture in Wood",
#                             isbn="978-050-03-4318-0", genre="arhitectura si design", publication_date="2016-02-3",
#                             pages=320,
#                             description="Some books are so beautifully produced and contain such superb images that even before one starts reading they have an entrancing quality that makes ownership essential. For anyone interested in design, architecture, cultures or travel [this book] is just such a volume . . . captivating. TES",
#                             price=151.99, stock=4, category=category1, promotion=promotion2,
#                             available_after_stock_empty=True)
# authors = Author.objects.filter(name="Will Pryce", origin_author="Londra, Regatul Unit")
# author8 = authors[0]
# authorbook10 = AuthorBook.objects.create(book=book8, author=author8)
# publisher5 = Publisher.objects.create(name="Editura Thames & Hudson Ltd", location="Londra - Regatul Unit")
# publisherbook8 = PublisherBook.objects.create(book=book8, publisher=publisher5)
#
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book9 = Book.objects.create(title="Wood: Architecture Today: 2018",
#                             isbn="978-849-93-6145-1", genre="arhitectura si design", publication_date="2018-10-29",
#                             pages=336,
#                             description="Wood is one of the best materials for construction on account of its versatility; this book presents the work of thirty international architects who work with wood.",
#                             price=202.00, stock=0, category=category1, promotion=promotion2,
#                             available_after_stock_empty=True)
# author9 = Author.objects.create(name="David Andreu", origin_author="Londra, Regatul Unit")
# authorbook11 = AuthorBook.objects.create(book=book9, author=author9)
# publisher6 = Publisher.objects.create(name="Editura Loft Publications", location="Londra - Regatul Unit")
# publisherbook9 = PublisherBook.objects.create(book=book9, publisher=publisher6)
#
# categories = Category.objects.filter(name="ARTA, ARHITECTURA SI FOTOGRAFIE")
# category1 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book10 = Book.objects.create(title="The Library.A World History",
#                              isbn="978-050-03-4288-6", genre="arta, istorie si arhitectura",
#                              publication_date="2013-10-01",
#                              pages=328,
#                              description="A library is not just a collection of books, but also the buildings that house them. As varied and inventive as the volumes they hold, such buildings can be much more than the dusty, dark wooden shelves found in mystery stories or the catacombs of stacks in the basements of academia."
#                                          " From the great dome of the Library of Congress, to the white façade of the Seinäjoki Library in Finland, to the ancient ruins of the library of Pergamum in modern Turkey, the architecture of a library is a symbol of its time as well as of its builders’ wealth, culture, and learning."
#                                          "Architectural historian James Campbell and photographer Will Pryce traveled the globe together, visiting and documenting over eighty libraries that exemplify the many different approaches to thinking about and designing libraries. The result of their travels, The Library: A World History is one of the first books to tell the story of library architecture around the world and through time in a single volume, from ancient Mesopotamia to modern China and from the beginnings of writing to the present day. As these beautiful and striking photos reveal, each age and culture has reinvented the library, molding it to reflect their priorities and preoccupations—and in turn mirroring the history of civilization itself. Campbell’s authoritative yet readable text recounts the history of these libraries, while Pryce’s stunning photographs vividly capture each building’s structure and atmosphere.Together, Campbell and Pryce have produced a landmark book—the definitive photographic history of the library and one that will be essential for the home libraries of book lovers and architecture devotees alike.",
#                              price=272.00, stock=0, category=category1, promotion=promotion2,
#                              available_after_stock_empty=True)
# author10 = Author.objects.create(name="James W.P.Campbell", origin_author="Regatul Unit")
# authorbook12 = AuthorBook.objects.create(book=book10, author=author10)
# publishers = Publisher.objects.filter(name="Editura Thames & Hudson Ltd", location="Londra - Regatul Unit")
# publisher5 = publishers[0]
# publisherbook10 = PublisherBook.objects.create(book=book10, publisher=publisher5)
#
#
# # aici incepe categoria "BELETRISTICA"
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book11 = Book.objects.create(title="Poezii",
#                              isbn="978-606-33-1840-5", genre="poezie",
#                              publication_date="2020-01-01",
#                              pages=192,
#                              description="Cartile de neuitat ale copilariei, capodoperele literaturii din toate timpurile reprezinta repere obligatorii in formarea gustului pentru lectura si in educatia cititorilor mai mici si mai mari."
#                                          "Cu ajutorul colectiei 'Lecturi scolare', elevii vor avea la indemana creatiile cele mai cunoscute si mai indragite ale scriitorilor clasici si moderni din literatura romana si universala, reprezentand bibliografia obligatorie pentru pregatirea de nota 10 la limba si literatura romana.",
#                              price=14.90, stock=5, category=category2, promotion=promotion1,
#                              available_after_stock_empty=True)
# author11 = Author.objects.create(name="Mihai Eminescu", origin_author="Humulesti, Romania")
# authorbook13 = AuthorBook.objects.create(book=book11, author=author11)
# publishers = Publisher.objects.filter(name="Editura Litera")
# publisher2 = publishers[0]
# publisherbook11 = PublisherBook.objects.create(book=book11, publisher=publisher2)
#
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book12 = Book.objects.create(title="Poezie. Proză",
#                              isbn="978-9975-54-178-7", genre="literatura romana clasica",
#                              publication_date="2015-01-01",
#                              pages=468,
#                              description="Viaţa lui se confundă cu opera, Eminescu n-are altă biografie... Rar se întâmplă ca un poet să fie sigilat de un destin, să ilustreze prin el însuşi bucuriile şi durerile existenţei şi de aceea multă vreme M. Eminescu va rămâne în poezia noastră nepereche - George Călinescu",
#                              price=21.90, stock=5, category=category2, promotion=promotion1,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Mihai Eminescu")
# author11 = authors[0]
# authorbook14 = AuthorBook.objects.create(book=book12, author=author11)
# publisher7 = Publisher.objects.create(name="Editura Prut", location="Bucuresti - Romania")
# publisherbook12 = PublisherBook.objects.create(book=book12, publisher=publisher7)
#
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book13 = Book.objects.create(title="Maitreyi",
#                              isbn="978-9731-04-619-8", genre="literatura romana clasica  - roman de dragoste",
#                              publication_date="2016-01-01",
#                              pages=199,
#                              description="“Maitreyi este un roman viu, substantial, cu o deschidere noua spre problematica omului modern (un spirit, intre altele, voiajor, confruntat in experientele lui existentiale cu mentalitati inradacinate in ceea ce antropologii numesc cultura de profunzime). Nu atat exotismul – desi nici acesta nu poate fi ignorat – este elementul inedit in romanul lui Eliade, ci un spirit nou de a gandi destinul omului in secolul al XX-lea, o vointa clara de sincronizare cu spiritul timpului.Un roman viabil, dupa mai bine de o jumatate de secol de cand a fost scris. Eliade incearca sa construiasca, dupa o formula a lui Bachelard, o mitologie a seductiei.” – Eugen Simion”Marea reusita a acestei carti vine, probabil, din incercarea de a transforma erosul in nodul gordian al celor doua aspiratii.",
#                              price=17.90, stock=4, category=category2, promotion=promotion1,
#                              available_after_stock_empty=True)
# author12 = Author.objects.create(name="Mircea Eliade", origin_author="Bucuresti, Romania")
# authorbook15 = AuthorBook.objects.create(book=book13, author=author12)
# publisher8 = Publisher.objects.create(name="Editura Cartex", location="Bucuresti - Romania")
# publisherbook13 = PublisherBook.objects.create(book=book13, publisher=publisher8)
#
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book14 = Book.objects.create(title="Poezii",
#                              isbn="978-6066-95-042-8", genre="poezie",
#                              publication_date="2016-01-01",
#                              pages=264,
#                              description="Intre poetii nostri, Octavian Goga ramane... cantaretul eului colectiv, al multimii anonime in suferinta..."
#                                          "Nobletea acestui sentiment, dezvoltat in lumina conceptiei crestine despre lume si viata, o putem numi demofilie. Octavian Goga e, prin excelenta, poetul nostru cel mai demofil.“ - Nichifor Crainic",
#                              price=18.00, stock=5, category=category2, promotion=promotion1,
#                              available_after_stock_empty=True)
# author13 = Author.objects.create(name="Octavian Goga", origin_author="Rasinari, Romania")
# authorbook16 = AuthorBook.objects.create(book=book14, author=author13)
# publisher9 = Publisher.objects.create(name="Editura Gramar", location="Bucuresti - Romania")
# publisherbook14 = PublisherBook.objects.create(book=book14, publisher=publisher9)
#
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Paste")
# promotion1 = promotions[0]
# book15 = Book.objects.create(title="Cel mai iubit dintre pamanteni, 3 volume",
#                              isbn="978-9737-88-363-6", genre="literatura romana contemporana - roman",
#                              publication_date="2017-01-01",
#                              pages=1048,
#                              description="Romanul 'Cel mai iubit dintre pamanteni', 1980 (trei volume, insumand peste 1200 de pagini) este prezentat cititorilor ca un text memorialistic si justificativ scris in inchisoare de un barbat cultivat (candva asistent la o catedra de filosofia culturii), Victor Petrini, acuzat de crima. El urmeaza sa apara in scurt timp in fata unei instante judecatoresti care l-ar putea condamna la inchisoare pe viata si incearca – sfatuit de avocat – sa-l castige pe judecator de partea sa, prin sinceritate, pentru a obtine o reducere a pedepsei."
#                                          "Se poate spune, asadar, ca romanul are factura unei marturisiri complete. Dar el este, in acelasi timp, si un raport catre Dumnezeu, intrucat Victor Petrini, sceptic in ceea ce priveste norocul sau in viata, se adreseaza, prin ceea ce scrie, nu numai instantei judecatoresti, ci si uneia mai inalte, care poate fi posteritatea sau Dumnezeu insusi.",
#                              price=65.00, stock=6, category=category2, promotion=promotion1,
#                              available_after_stock_empty=True)
# author14 = Author.objects.create(name="Marin Preda", origin_author="Siliștea-Gumești, Romania")
# authorbook17 = AuthorBook.objects.create(book=book15, author=author14)
# publishers = Publisher.objects.filter(name="Editura Cartex")
# publisher8 = publishers[0]
# publisherbook15 = PublisherBook.objects.create(book=book15, publisher=publisher8)
#
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book16 = Book.objects.create(title="Viata ca o prada",
#                              isbn="978-9737-88-398-8", genre="literatura romana contemporana - roman",
#                              publication_date="2020-01-01",
#                              pages=304,
#                              description="[…] Dincolo de valoarea ei literara, 'Viata ca o prada' este, indiscutabil, un laborator. Scriitorul a simtit ca trebuie sa ne dea noua, cititorilor sai, anume explicatii, sa ne introduca in secretele unei opere atat de larg cunoscuta si recunoscuta ‒ ceea ce si face. Dar, ce ciudat, desi iesim din aceasta carte cu un spor indubitabil de cunoastere, o parasim, chiar si la o a doua atenta lectura, cu noi perplexitati ‒ si aceste noi tulburari sporesc valoarea artistica a cartii. […]"
#                                          "Ca 'Viata ca o prada' constituie romanul unei vocatii scriitoricesti este indiscutabil; dar este tot atat de adevarat ca este si aventura unei constiinte ‒ procesul formarii constiintei, iar aceasta se intreaba si ne intreaba: Cum traim? De ce traim astfel? Cum sa traim? Un mare scriitor ‒ cu atat mai mult un scriitor national ‒ nu se poate multumi sa ne povesteasca intamplari, fie ele si foarte interesante si adevarate pe deasupra, ci el incearca sa-si puna, si sa rezolve in felul sau, intrebarile esentiale ale existentei. Daca eroul cartii trece prin intamplari extrem de dificile, cu o liniste interioara uneori de-a dreptul stupefianta, aceasta se intampla pentru ca el este in posesia unei certitudini: vocatia sa literara. Sigur ca eroul si naratorul nu pot fi confundati, intre ei intinzandu-se un mare spatiu al realizarii, dar un scriitor nu poate extinde asupra eroului sau o liniste pe care acesta n-a avut-o. - Paul Georgescu",
#                              price=33.00, stock=4, category=category2, promotion=promotion2,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Marin Preda")
# author14 = authors[0]
# authorbook18 = AuthorBook.objects.create(book=book16, author=author14)
# publishers = Publisher.objects.filter(name="Editura Cartex")
# publisher8 = publishers[0]
# publisherbook16 = PublisherBook.objects.create(book=book16, publisher=publisher8)
#
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book17 = Book.objects.create(title="Marele singuratic",
#                              isbn="978-9737-88-370-4", genre="literatura romana contemporana - roman",
#                              publication_date="2017-01-01",
#                              pages=384,
#                              description="Ii datoram lui Marin Preda mai mult decat majoritatii istoricilor si filosofilor contemporani care reflecteaza asupra destinului romanesc. Efectul relecturii romanelor, nuvelelor, scrierilor sale in general, este cel mai eficient antidot impotriva mitului 'desertului cultural' de care a fost suspectat spatiul romanesc in cea de a doua jumatate a secolului XX."
#                                          "Cu 'Marele singuratic' (1972), Marin Preda revine la romanescul traditional (istoria unui destin, prezentarea mediilor sociale, conflict moral, intriga sentimentala etc.) cu o experienta noua, totusi, in ce priveste tehnica epica (eseul patrunde masiv, naratiunea este din cand in cand sparta) si cu dorinta de a trata din unghiul sau de vedere o problema dezbatuta de toata literatura moderna: raportul dintre personalitatea individului si determinismul istoriei. Tema mai intima a cartii este incercarea de a iesi din istorie."
#                                          "Angajare, actiune (cu riscul esecului) sau retragere, solitudine, boicot (o vorba ce nu place prozatorului!) al istoriei? - Eugen Simion",
#                              price=25.00, stock=3, category=category2, promotion=promotion2,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Marin Preda")
# author14 = authors[0]
# authorbook19 = AuthorBook.objects.create(book=book17, author=author14)
# publishers = Publisher.objects.filter(name="Editura Cartex")
# publisher8 = publishers[0]
# publisherbook17 = PublisherBook.objects.create(book=book17, publisher=publisher8)
#
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book18 = Book.objects.create(title="Morometii. Volumele I si II (editia 2019)",
#                              isbn="978-9737-88-385-8", genre="literatura romana contemporana - roman",
#                              publication_date="2019-01-01",
#                              pages=1024,
#                              description="Prin 'Morometii', Marin Preda ne vorbeste de tarani cum nu se mai vorbise pana la el. Intr-o literatura saturata, aparent, de materia vietii taranesti, aceasta aparitie parea imposibila. Izvorul parea epuizat si o adevarata campanie pragmatica impotriva literaturii cu subiecte rurale contribuise si ea, decisiv, la consolidarea impresiei. Marin Preda ne descopera totusi o lume inedita, scaldata intr-o lumina solara care alunga tenebrele. - Mihai Ungureanu"
#                                          "'Morometii' e povestea nu numai a taranului roman si a schimbarilor prin care acesta trece, ci o poveste actuala a romanului in general, al carui caracter este subliniat de umorul negru si conexiunea cu viata la sat.",
#                              price=65.00, stock=5, category=category2, promotion=promotion2,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Marin Preda")
# author14 = authors[0]
# authorbook20 = AuthorBook.objects.create(book=book18, author=author14)
# publishers = Publisher.objects.filter(name="Editura Cartex")
# publisher8 = publishers[0]
# publisherbook18 = PublisherBook.objects.create(book=book18, publisher=publisher8)
#
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book19 = Book.objects.create(title="Imposibila intoarcere",
#                              isbn="978-9737-88-391-9", genre="literatura romana contemporana - publicistica",
#                              publication_date="2020-01-01",
#                              pages=256,
#                              description="Literatura contemporana simte mai mult ca oricand nevoia unor piscuri stabile, a unor centre in jurul carora sa fie posibila actiunea de referinta. Este bine sa ne gandim ca Marin Preda este unul dintre acestea. Discutata fie ca o carte de publicistica sau jurnal - prin ceea ce contine ca eveniment si confesiune - fie ca o carte de literatura cu unele implicatii teoretice in care autorul se substituie celebrului Moromete prin masura caruia judeca civilizatia, fie prin ceea ce contine ca anecdotica, Imposibila intoarcere este toate acestea laolalta si ceva mai mult, o partitura in care tema povestitorului, ce apare pentru prima data, se amplifica punand toate celelalte motive in umbra. Ea se asaza rotund, intr-o filosofie originala prin sistem ce-l indica pe Marin Preda drept unul din momentele importante ale spiritualitatii romane.",
#                              price=25.00, stock=3, category=category2, promotion=promotion2,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Marin Preda")
# author14 = authors[0]
# authorbook21 = AuthorBook.objects.create(book=book19, author=author14)
# publishers = Publisher.objects.filter(name="Editura Cartex")
# publisher8 = publishers[0]
# publisherbook19 = PublisherBook.objects.create(book=book19, publisher=publisher8)
#
# categories = Category.objects.filter(name="BELETRISTICA")
# category2 = categories[0]
# promotions = Promotion.objects.filter(name="Promotie de Primavara")
# promotion2 = promotions[0]
# book20 = Book.objects.create(title="Risipitorii",
#                              isbn="978-9737-88-372-8", genre="literatura romana contemporana - roman",
#                              publication_date="2018-01-01",
#                              pages=440,
#                              description="„Este un bun roman psihologic, in care fiecare caz (personaj) in parte reprezinta o ilustrare a esecului temporar sau a ratarii posibile, acestea fiind, pentru Marin Preda, consecinte sau forme de manifestare ale risipirii de sine. De aici, desigur, si titlul romanului. Intreaga scriere este gandita ca o demonstratie a acestei simple axiome morale, expusa de unul dintre personaje: «Tinerii [...] isi risipesc, intr-un fel sau altul, tineretea fara s-o stie.» Principala cauza a acestui fenomen derutant ar fi inefabilul psihologic, manifestat sub forma unui rau obscur al firii, care obstrueaza implinirea de sine si care se poate manifesta prin acte sau actiuni ciuntite, neduse pana la capat, prin crize personale, prin esecuri anticipate sau programate in mod obscur. Scriitorul aglomereaza mai multe cazuri, mai multe enigma morale sau psihologice, pe care le analizeaza, carora le descopera necunoscutele pentru a le gasi rezolvarea.“ - Oana Soare",
#                              price=35.00, stock=4, category=category2, promotion=promotion2,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Marin Preda")
# author14 = authors[0]
# authorbook22 = AuthorBook.objects.create(book=book20, author=author14)
# publishers = Publisher.objects.filter(name="Editura Cartex")
# publisher8 = publishers[0]
# publisherbook20 = PublisherBook.objects.create(book=book20, publisher=publisher8)
#
#
# #aici incepe categoria BIOGRAFII SI MEMORIALISTICA
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotion3 = Promotion.objects.create(name="Reduceri finale de sezon", period_of_promotion="15.04.2021-15.06.2021")
# book21 = Book.objects.create(title="Povestea mea",
#                              isbn="978-6063-33-008-7", genre="autobiografie si memorii",
#                              publication_date="2018-09-01",
#                              pages=624,
#                              description="AUTOBIOGRAFIA FOSTEI PRIME-DOAMNE A STATELOR UNITE ALE AMERICII – SINCERĂ, PUTERNICĂ, FASCINANTĂ"
#                                          "În această carte, pentru prima dată, Michele Obama descrie primii ani ai căsniciei sale, când încerca să găsească echilibrul între carieră și ascensiunea rapidă în politică a soțului ei. Ne dezvăluie detalii intime din discuțiile pe care le-au purtat înainte de candidatura lui Barack Obama la președinție și impresii despre rolul pe care ea l-a jucat în campania lui electorală, ca o figură populară, dar și foarte des criticată. Cu un stil narativ grațios, presărat cu umor și o sinceritate neobișnuită, Michelle Obama ne oferă o poveste vie din culisele vieții ei, relatând secvențe neștiute despre lansarea istorică a familiei ei pe scena internațională, precum și din viața lor ca familie prezidențială, de-a lungul celor opt ani spectaculoși petrecuți la Casa Albă – în care și-a descoperit propria țară, iar America a descoperit-o pe ea."
#                                          "'Povestea mea' ne poartă prin bucătăriile modeste din Iowa și prin sălile de bal de la Palatul Buckingham, prin momente de durere sfâșietoare și ambiție fără limite, aducându-ne aproape de sufletul unei figuri unice, inovatoare a istoriei, în timp ce se străduiește să trăiască autentic, punându-și forța și vocea în serviciul unor idealuri mai înalte. Spunându-și povestea cu sinceritate și îndrăzneală, ne aruncă o provocare nouă, celorlalți: Cine suntem și cine dorim să devenim?"
#                                          "„Sunt multe lucruri pe care încă nu le știu despre America, despre viață, despre ce ar putea să aducă viitorul. Dar mă cunosc pe mine însămi. Tatăl meu, Fraser, m‑a învățat să muncesc din greu, să zâmbesc des și să mă țin de cuvânt mereu. Mama mea, Marian, mi‑a arătat cum să judec singură lucrurile și cum să‑mi fac vocea auzită. Împreună, în apartamentul nostru înghesuit din sudul orașului Chicago, ei doi m‑au învățat să descopăr ceea ce e important în povestea noastră, în povestea mea, în marea poveste a țării noastre. Chiar dacă nu e totul frumos sau perfect. Chiar dacă este mai real decât ai vrea să fie. Povestea ta este tot ce ai, tot ce vei avea vreodată. Este ceva ce merită prețuit.”",
#                              price=22.99, stock=3, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
# author15 = Author.objects.create(name="Michelle Obama", origin_author="Chicago, Statele Unite Ale Americii")
# authorbook23 = AuthorBook.objects.create(book=book21, author=author15)
# publishers = Publisher.objects.filter(name="Editura Litera")
# publisher2 = publishers[0]
# publisherbook21 = PublisherBook.objects.create(book=book21, publisher=publisher2)
#
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book22 = Book.objects.create(title="Povestea mea. Jurnal",
#                              isbn="978-6063-34-528-9", genre="jurnal, autobiografie si memorii",
#                              publication_date="2019-11-01",
#                              pages=208,
#                              description="'E nevoie de putere că să-ți dai voie să devii cunoscut și auzit, să-ți asumi propria poveste și să-ți folosești adevărată voce. Și e nevoie de bunăvoința pentru a fi dispus cunoști și asculți pe ceilalți.' Michelle Obama, 'Povestea mea'"
#                                          "'Povestea mea. Jurnal' - ca sa-ti descoperi propria voce.Un jurnal în care să îți notezi gândurile, progresele, dorințele. Un jurnal îmbogățit cu citate și gânduri, în stilul inconfundabil al fostei prime-doamne a Americii, Michelle Obama. Gândește-te la acest jurnal că la tovarășul tău în propria călătorie spre devenire. Bazate pe memoriile devenite bestseller ale lui Michelle Obama, aceste pagini includ întrebări ce te provoacă și te îndeamnă să descoperi și să redescoperi acel lucru care va fi adevărat pentru ține și doar pentru ține — povestea ta."
#                                          "În paginile jurnalului, vei descoperi o ocazie nu doar de a te implica în experiențele care te-au adus unde ești astăzi, dar și de a te simți încurajat să faci acei pași următori, oriunde te vor duce aceștia. ",
#                              price=22.99, stock=4, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Michelle Obama")
# author15 = authors[0]
# authorbook24 = AuthorBook.objects.create(book=book22, author=author15)
# publishers = Publisher.objects.filter(name="Editura Litera")
# publisher2 = publishers[0]
# publisherbook22 = PublisherBook.objects.create(book=book22, publisher=publisher2)
#
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book23 = Book.objects.create(title="Pamantul fagaduintei",
#                              isbn="978-6063-36-679-6", genre="autobiografie si memorii",
#                              publication_date="2020-11-17",
#                              pages=850,
#                              description="Nimic nu se compară cu sentimentul pe care-l ai când termini de scris o carte și sunt mândru de aceasta pe care tocmai am finalizat-o. Mi-am petrecut ultimii câțiva ani reflectând asupra președinției mele, iar în 'PĂMÂNTUL FĂGĂDUINȚEI' am încercat să redau cu sinceritate povestea campaniei mele prezidențiale și timpul în care am fost în funcție: evenimentele-cheie și oamenii care au stat la baza lor; părerea mea despre lucrurile care mi-au ieșit și greșelile pe care le-am făcut; și forțele de ordin politic, economic și cultural cu care echipa mea și cu mine ne-am confruntat atunci – și cu care, ca națiune, încă avem de-a face. De asemenea, am încercat să redau cititorilor călătoria intimă pe care Michelle și cu mine am făcut-o în acei ani, cu toate momentele abisale și cele înălțătoare."
#                                          "Și în cele din urmă, în aceste vremuri în care America trece prin niște momente decisive atât de semnificative, cartea oferă câteva dintre părerile generale pe care le am despre cum putem să vindecăm diviziunile din țara noastră de-acum încolo și cum să facem ca democrația să fie în folosul tuturor – o sarcină care nu va depinde de un anume președinte, ci de noi toți, ca cetățeni implicați. Dincolo de a fi o lectură plăcută și informativă, sper mai mult decât orice ca această carte să inspire tinerii din toate colțurile țării – și ale lumii – să preia ștafeta, să-și facă vocea auzită și să întreprindă tot ce ține de ei ca să schimbe lumea în bine.” Barack Obama"
#                                          "Într-un stil original, Barack Obama istorisește în PĂMÂNTUL FĂGĂDUINȚEI povestea odiseei sale neverosimile de la un tânăr aflat în căutarea propriei identități la liderul lumii libere, descriind prin amănunte deosebit de intime atât educația sa politică, cât și momentele cruciale din primul mandat al președinției sale istorice – o perioadă de frământări și de transformări profunde."
#                                          "Obama relatează de o manieră emoționantă și foarte personală cum s-au scris pagini întregi de istorie și îi poartă pe cititori într-o călătorie irezistibilă, pornind de la cele mai timpurii ambiții politice ale sale, la victoria în prima rundă din alegeri în Iowa, care a reprezentat un punct de cotitură și care a demonstrat forța activismului la firul ierbii, până la noaptea istorică din 4 noiembrie 2008, când a fost ales al 44-lea președinte al Statelor Unite ale Americii, devenind primul afro-american care ocupa cea mai înaltă funcție în stat."
#                                          "'PĂMÂNTUL FĂGĂDUINȚEI' este o relatare deosebit de intimă și de introspectivă – povestea unui om care a pus pariu cu istoria, credința unui organizator al comunității, testată pe scena mondială. Obama vorbește cu franchețe despre cum a împăcat candidatura la fotoliul prezidențial ca afro-american în care își pusese speranțele o generație îmbărbătată de mesaje de „speranță și schimbare”, cu provocările de ordin moral pe care le presupun deciziile de mare miză ce trebuie luate. Relatează în mod deschis despre forțele care i s-au opus atât în țară cât și în străinătate, despre impactul pe care traiul la Casa Albă l-a avut asupra soției și a fiicelor sale, și nu-i este teamă să-și dezvăluie îndoielile de sine și dezamăgirile. Cu toate acestea, păstrează credința nestrămutată în ideea că e întotdeauna loc pentru progres în mărețul și perpetuul experiment american.",
#                              price=46.95, stock=5, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
# author16 = Author.objects.create(name="Barack Obama", origin_author="Honolulu, Hawaii, Statele Unite Ale Americii")
# authorbook25 = AuthorBook.objects.create(book=book23, author=author16)
# publishers = Publisher.objects.filter(name="Editura Litera")
# publisher2 = publishers[0]
# publisherbook23 = PublisherBook.objects.create(book=book23, publisher=publisher2)
#
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book24 = Book.objects.create(title="Dreams From My Father",
#                              isbn="978-1847-67-094-6", genre="autobiografie si memorii",
#                              publication_date="2008-01-01",
#                              pages=464,
#                              description="The son of a black African father and a white American mother, Barack Obama was only two years old when his father walked out on the family. This memoir illuminates not only Obama's journey, but also our universal desire to understand our history, and what makes us the people we are.",
#                              price=49.99, stock=0, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Barack Obama")
# author16 = authors[0]
# authorbook26 = AuthorBook.objects.create(book=book24, author=author16)
# publisher10 = Publisher.objects.create(name="Editura Canongate Books Ltd", location=" Edinburgh, Regatul Unit")
# publisherbook24 = PublisherBook.objects.create(book=book24, publisher=publisher10)
#
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book25 = Book.objects.create(title="O autobiografie",
#                              isbn="978-6063-36-019-0", genre="autobiografie si memorii",
#                              publication_date="2020-06-01",
#                              pages=768,
#                              description="Publicată în 1977, la un an după moartea autoarei, 'O autobiografie' te ține cu sufletul la gură asemenea romanelor ei. Scrise cu verva, umorul și nonșalanța bine-cunoscute, frânturile de amintiri pe care Agatha Christie alege să le dezvăluie cititorului – sugerând chiar din titlu că aceasta este o narațiune foarte personală, nu Autobiografia cu majusculă – sunt deopotrivă semnificative pentru memoria afectivă a scriitoarei, cât și pentru epoca pe care a trăit-o, o perioadă framân - tată din istoria lumii, de la sfârșitul epocii victoriene, când lumea părea să aibă răbdare, la cele două războaie mondiale și apoi la deplina maturitate și împlinire ca scriitoare din anii postbelici. Copilăria și adolescența, apoi prima căsătorie și nașterea fiicei sale Rosalind, moartea primului soț (al cărui nume îl poartă) și a doua căsătorie, cu arheologul Sir Max Mallowan, pe care l-a însoțit în expedițiile de săpături în Irak, stau mărturie pentru o viață pe care Agatha Christie a stiut să o trăiască din plin, cu toate suișurile și coborâșurile ei."
#                                          "„Așa că am de gând să mă desfăt cu amintirile mele – pe îndelete, fără nici o grabă –, așternându-le din când în când pe hârtie. E o muncă ce-mi va lua mulți ani, probabil. Dar de ce o numesc muncă? De fapt, e un privilegiu. Cândva am văzut un pergament chinezesc foarte vechi de care m-am îndrăgostit pe loc. Înfățișa un bătrân care ședea sub un copac și juca moara. Desenul se intitula Bătrân savurând bucuria tihnei. Mi-a rămas întipărit în minte pe vecie.“ Agatha Christie",
#                              price=26.46, stock=4, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
# author17 = Author.objects.create(name="Agatha Christie", origin_author="Torquay, Regatul Unit")
# authorbook27 = AuthorBook.objects.create(book=book25, author=author17)
# publishers = Publisher.objects.filter(name="Editura Litera")
# publisher2 = publishers[0]
# publisherbook25 = PublisherBook.objects.create(book=book25, publisher=publisher2)
#
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book26 = Book.objects.create(title="Einstein. Viata si universul sau",
#                              isbn="978-6067-22-351-4", genre="biografii si memorii",
#                              publication_date="2019-01-01",
#                              pages=720,
#                              description="Scrisa de autorul bestsellerului 'Leonardo da Vinci', aceasta este prima biografie completa a lui Albert Einstein de cand au devenit accesibile toate documentele sale."
#                                          "Cum a funcționat mintea sa? Ce a făcut ca el să fie un geniu? Biografia lui Isaacson ne arată modul în care imaginația lui științifică a izvorât din natura rebelă a personalității sale. Fascinanta sa poveste este o dovadă a legăturii dintre creativitate și libertate."
#                                          "Walter Isaacson a surprins un Einstein în integralitatea sa. Cu un stil ușor, care maschează o mare atenție la detalii și acuratețe științifică, Isaacson ne poartă într-o călătorie prin viața, mintea și știința omului care a schimbat viziunea noastră asupra universului. — BRIAN GREEN, profesor de fizică la Universitatea Columbia și autorul volumului 'The Fabric of Cosmos'",
#                              price=99.00, stock=2, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
# author18 = Author.objects.create(name="Walter Isaacson", origin_author="New Orleans, Louisiana, Statele Unite")
# authorbook28 = AuthorBook.objects.create(book=book26, author=author18)
# publisher11 = Publisher.objects.create(name="Editura Publica", location="Bucuresti, Romania")
# publisherbook26 = PublisherBook.objects.create(book=book26, publisher=publisher11)
#
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book27 = Book.objects.create(title="Petru cel mare. Viața și lumea lui",
#                              isbn="978-6065-87-280-6", genre="biografii si memorii",
#                              publication_date="2016-05-26",
#                              pages=808,
#                              description="Magnifica poveste a lui Petru cel Mare (1672-1725)"
#                                          "Robert K. Massie a câștigat Premiul Pulitzer pentru această lucrare de excepție, în care prezintă viața unui personaj unic în istoria lumii."
#                                          "Când soarele se reflecta în sutele de cupole aurii, încununate de o pădure de cruci de aur, Moscova devenea cel mai frumos oraș din lume, sub lumina copleșitoare a apusului. În centrul lui, pe o colină, se afla cetatea Kremlinului, mândria Moscovei, cu cele trei magnifice catedrale ale sale, clopotnița impunătoare, palatele superbe, capele și sute de case. Aici a venit pe lume, la 30 mai 1672, băiatul care avea să se transforme într-o legendă. Viața lui s-a desfășurat în Rusia și Europa pe fundalul monumental al secolelor XVII și XVIII. El este cel care a scos Rusia din negura Evului Mediu și a transformat-o într-un stat modern, conferindu-i puterea pe care a moștenit-o până în zilele noastre."
#                                          "Cartea surprinde convingerile, capriciile, curiozitățile și acțiunile acestei personalități complexe. Autorul urmărește îndeaproape relațiile cu cei pe care Petru cel Mare i-a iubit cel mai mult: Ecaterina, iubita sa tandră, şi Menşikov, prinţul fermecător şi lipsit de scrupule care a ajuns la putere prin intermediul său. Un loc special îl ocupă relaţia dificilă cu fiul său Aleksei, care a fost judecat şi condamnat la moarte în 1718. De asemenea, lucrarea ne introduce în lumea rusească schimbată iremediabil, uneori în mod violent, de reformele lui Petru cel Mare. Viziunea sa asupra vieții politice, sociale, culturale și religioase a avut o influenţă îndelungată asupra societăţii ruseşti. Stăpânit de obsesia pentru mare, Petru a luat decizia de a construi măreața flotă rusească. A urmărit consolidarea forței statului prin înființarea Sankt-Petersburgului și crearea unei armate de neînvins.",
#                              price=119.92, stock=3, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
# author19 = Author.objects.create(name="Robert K. Massie", origin_author="Kentucky, Statele Unite")
# authorbook29 = AuthorBook.objects.create(book=book27, author=author19)
# publisher12 = Publisher.objects.create(name="Grupul Editorial All", location="Bucuresti, Romania")
# publisherbook27 = PublisherBook.objects.create(book=book27, publisher=publisher12)
#
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book28 = Book.objects.create(title="Ecaterina cea Mare. Portretul unei femei",
#                              isbn="978-6065-87-423-7", genre="biografii si memorii",
#                              publication_date="2018-01-01",
#                              pages=564,
#                              description="O istorie a puterii si a pasiunii de gen feminin.Lucrarea despre Ecaterina cea Mare este a doua carte din monumentala trilogie pe care renumitul istoric american Robert K. Massie a dedicat-o familiei imperiale a Romanovilor, dinastia care a condus Rusia timp de mai bine de trei secole, pana in 1917. Editura ALL a publicat, in conditii grafice de exceptie, si prima parte a trilogiei, Petru cel Mare. Viata si lumea lui, volum distins cu Premiul Pulitzer. Pastrand acelasi stil care imbina valoarea documentara cu talentul narativ, istoria Ecaterinei cea Mare se contureaza ca „o superba poveste, spusa de un desavarsit povestitor” (The Wall Street Journal). Cu alte cuvinte, primim toate informatiile demne de o desavarsita biografie istorica intr-o maniera care o face la fel de fascinanta ca un roman."
#                                          "Ducele de Buckingham, ambasadorul britanic in Rusia in perioada 1762-1765, a schitat perfect portretul Ecaterinei – „Poate cea mai potrivita descriere a ei ar fi ca este, deopotriva, femeie si imparateasa.”, iar biografia lui Massie ii reflecta pe deplin viziunea. Lucrarea urmareste destinul acestei femei remarcabile pe parcursul a 7 sectiuni principale, care reunesc 73 de capitole."
#                                          "Massie a fost intotdeauna un biograf cu instincte de scriitor. El intelege actiunea – destinul – ca pe o functie a personajului, iar perspectiva narativa pe care o stabileste si o mentine e de natura sa-l faca pe cititor sa creada ca figura somptuoasa a Ecaterinei cea Mare prinde viata, ca printr-o magie, chiar sub ochii lui. – The New York Times Book Review"
#                                          "Aceasta lucrare nu se adreseaza numai istoricilor impatimiti si niciun cititor n-ar trebui sa se sperie de volumul ei. Robert Massie a scris o biografie cuprinzatoare, pe care o citesti ca pe un roman si pe care cu greu o mai lasi jos din mana. – The Washington Times",
#                              price=108.91, stock=3, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
#
# books = Book.objects.filter(title="Ecaterina cea Mare. Portretul unei femei")
# book28 = books[0]
# authors = Author.objects.filter(name="Robert K. Massie")
# author19 = authors[0]
# authorbook30 = AuthorBook.objects.create(book=book28, author=author19)
#
# publishers = Publisher.objects.filter(name="Grupul Editorial All")
# publisher12 = publishers[0]
# publisherbook28 = PublisherBook.objects.create(book=book28, publisher=publisher12)
#
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book29 = Book.objects.create(title="The Kennedy Curse",
#                              isbn="978-1787-46-535-0", genre="biografii si memorii",
#                              publication_date="2021-01-01",
#                              pages=448,
#                              description="Across decades and generations, the Kennedys have been a family of charismatic adventurers, raised to take risks and excel. Their name is synonymous with American royalty. Their commitment to public service is legendary. But, for all the successes, the family has been blighted by assassinations, fatal accidents, drug and alcohol abuse and sex scandals."
#                                          "To this day, the Kennedys occupy a unique, contradictory place in the world's imagination: at once familiar and unknowable; charmed and cursed. The Kennedy Curse is a revealing, fascinating account of America's most famous family, as told by the world's most trusted storyteller.",
#                              price=47.00, stock=4, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
# author20 = Author.objects.create(name="James Patterson", origin_author="New York, Statele Unite")
#
# books = Book.objects.filter(title="The Kennedy Curse")
# book29 = books[0]
# authors = Author.objects.filter(name="James Patterson", origin_author="New York, Statele Unite")
# author20 = authors[0]
# authorbook31 = AuthorBook.objects.create(book=book29, author=author20)
#
# publisher13 = Publisher.objects.create(name="Editura Cornerstone", location="Houston Texas, Statele Unite")
# publisherbook29 = PublisherBook.objects.create(book=book29, publisher=publisher13)
#
# categories = Category.objects.filter(name="BIOGRAFII SI MEMORIALISTICA")
# category3 = categories[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book30 = Book.objects.create(title="Jurnalul Annei Frank",
#                              isbn="978-9735-07-078-6", genre="jurnal, autobiografie si memorii",
#                              publication_date="2021-01-01",
#                              pages=392,
#                              description="Anne Frank este un nume familiar chiar și pentru cei care nu au citit Jurnalul. Îl poartă străzi, școli de pe toate meridianele. Îl poartă mult vizitatul muzeu din Amsterdam și fundația care l-a organizat în imobilul unde se afla Anexa secretă. Anne Frank și jurnalul ei figurează pe mai toate listele de excelență ale veacului XX privitoare la personalități și la cărți – Cei mai importanți oameni ai secolului, Cele mai bune cărți publicate în secolul XX, Operele literare definitorii pentru același secol, ca să numim doar câteva din multele topuri stabilite de specialiști, de ziariști ori chiar de marele public."
#                                          "Din 1947, când s-a publicat pentru prima oară în Țările de Jos, Jurnalul Annei Frank a fost tradus în peste 65 de limbi. S-a vândut în întreaga lume în peste 30 de milioane de exemplare. La celebritatea Jurnalului au contribuit neîndoielnic adaptările sale teatrale și cinematografice, faptul că a devenit materie de studiu în multe școli din lume, că a fost obiectul unor analize subtile, dar și ținta unor vehemente atacuri.Anne Frank a lăsat cea mai frapantă mărturie umană din timpul celui de-al Doilea Război Mondial. Mai mult, a dat un chip, o voce, un nume milioanelor de victime inocente – fără chip, fără voce, fără nume – ale barbariei naziste."
#                                          "Anne Frank nu e o simplă autoare: împreună cu cartea ei, a devenit un simbol.",
#                              price=45.00, stock=4, category=category3, promotion=promotion3,
#                              available_after_stock_empty=True)
# author21 = Author.objects.create(name="Anne Frank", origin_author="Amsterdam, Olanda")
#
# books = Book.objects.filter(title="Jurnalul Annei Frank")
# book30 = books[0]
# authors = Author.objects.filter(name="Anne Frank")
# author21 = authors[0]
# authorbook32 = AuthorBook.objects.create(book=book30, author=author21)
#
# publisher14 = Publisher.objects.create(name="Editura Humanitas", location="Bucuresti, Romania")
# publisherbook30 = PublisherBook.objects.create(book=book30, publisher=publisher14)
#
#
# #aici incepe categoria CALCULATOARE/IT
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book31 = Book.objects.create(title="Python Programming: Python Programming from Beginner to Intermediate in 7 Days",
#                              isbn="978-1794-21-701-0", genre="programare calculatoare",
#                              publication_date="2019-01-16",
#                              pages=106,
#                              description="Quickstart guide for Python ProgrammingPython is an incredibly versatile and powerful programming language, but only if you know how to use it Need to learn Python fast?Python can be used to create just about any kind of programming project you can"
#                              "Python has been around for a long time, but has evolved over the years. Save yourself the headache and frustration of trying to use a guide that just isn’t up to date anymore! Brand new and fully up to date, this guide shows you exactly what you need to do to start programming in Python today!",
#                              price=66.00, stock=0, category=category4, promotion=promotion3,
#                              available_after_stock_empty=True)
# author22 = Author.objects.create(name="Cal Baron", origin_author="Statele Unite")
# authorbook33 = AuthorBook.objects.create(book=book31, author=author22)
# publisher15 = Publisher.objects.create(name="Editura Independently Published", location="Statele Unite")
# publisherbook31 = PublisherBook.objects.create(book=book31, publisher=publisher15)
#
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book32 = Book.objects.create(title="Clean Python - Elegant Coding in Python",
#                              isbn="978-1484-24-877-5", genre="programare calculatoare",
#                              publication_date="2019-05-21",
#                              pages=267,
#                              description="Discover the right way to code in Python. This book provides the tips and techniques you need to produce cleaner, error-free, and eloquent Python projects."
#                              "Your journey to better code starts with understanding the importance of formatting and documenting your code for maximum readability, utilizing built-in data structures and Python dictionary for improved maintainability, and working with modules and meta-classes to effectively organize your code. You will then dive deep into the new features of the Python language and learn how to effectively utilize them. Next, you will decode key concepts such as asynchronous programming, Python data types, type hinting, and path handling. Learn tips to debug and conduct unit and integration tests in your Python code to ensure your code is ready for production. The final leg of your learning journey equips you with essential tools for version management, managing live code, and intelligent code completion."
#                              "After reading and using this book, you will be proficient in writing clean Python code and successfully apply these principles to your own Python projects.",
#                              price=173.00, stock=0, category=category4, promotion=promotion3,
#                              available_after_stock_empty=True)
# author23 = Author.objects.create(name="Kapil Sunil", origin_author="India")
# authorbook34 = AuthorBook.objects.create(book=book32, author=author23)
# publisher16 = Publisher.objects.create(name="Editura APress", location="New York, Statele Unite")
# publisherbook32 = PublisherBook.objects.create(book=book32, publisher=publisher16)
#
#
#
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book33 = Book.objects.create(title="Think Python, 2nd Edition",
#                              isbn="978-1491-93-936-9", genre="programare calculatoare",
#                              publication_date="2015-12-15",
#                              pages=292,
#                              description="This updated second edition has a more direct focus on Python 3, along with simplified installation instructions."
#                                          " You'll also get added coverage of more topics, including list comprehensions and additional data structures.",
#                              price=207.00, stock=0, category=category4, promotion=promotion3,
#                              available_after_stock_empty=True)
# author24 = Author.objects.create(name="Allen B. Downey", origin_author="Statele Unite")
# authorbook35 = AuthorBook.objects.create(book=book33, author=author24)
# publisher17 = Publisher.objects.create(name="Editura  O'Reilly Media, Inc",
#                                        location="Sebastopol, California, Statele Unite")
# publisherbook33 = PublisherBook.objects.create(book=book33, publisher=publisher17)
#
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book34 = Book.objects.create(title="Python for Software Design: How to Think Like a Computer Scientist ",
#                              isbn="978-0521-72-596-5", genre="programare calculatoare",
#                              publication_date="2009-03-01",
#                              pages=270,
#                              description="'Python for Software Design' is a concise introduction to software design using the Python programming language. Intended for people with no programming experience, this book starts with the most basic concepts and gradually adds new material. Some of the ideas students find most challenging, like recursion and object-oriented programming, are divided into a sequence of smaller steps and introduced over the course of several chapters."
#                                          "The focus is on the programming process, with special emphasis on debugging. The book includes a wide range of exercises, from short examples to substantial projects, so that students have ample opportunity to practice each new concept. Exercise solutions and code examples are available from thinkpython.com, along with Swampy, a suite of Python programs that is used in some of the exercises.",
#                              price=231.00, stock=0, category=category4, promotion=promotion3,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Allen B. Downey")
# author24 = authors[0]
# authorbook36 = AuthorBook.objects.create(book=book34, author=author24)
# publisher18 = Publisher.objects.create(name="Editura Cambridge University Press", location="Cambridge, Regatul Unit")
# publisherbook34 = PublisherBook.objects.create(book=book34, publisher=publisher18)
#
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book35 = Book.objects.create(title="Learn Python 3 the Hard Way",
#                              isbn="978-0134-69-288-3", genre="programare calculatoare",
#                              publication_date="2017-06-27",
#                              pages=320,
#                              description="Readers will learn Python by working through 52 brilliantly crafted exercises. Read them. Type their code precisely. (No copying and pasting!) Fix the mistakes. Watch the programs run. Includes 5+ hours of video where Shaw shows how to break, fix, and debug code.",
#                              price=182.00, stock=0, category=category4, promotion=promotion3,
#                              available_after_stock_empty=True)
# author25 = Author.objects.create(name="Zed A. Shaw", origin_author="Statele Unite")
# authorbook37 = AuthorBook.objects.create(book=book35, author=author25)
# publisher19 = Publisher.objects.create(name="Editura Pearson Education(US)", location="New Jersey, Statele Unite")
# publisherbook35 = PublisherBook.objects.create(book=book35, publisher=publisher19)
#
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book36 = Book.objects.create(title="Clean Code-A Handbook Of Agile Software Craftmanship",
#                              isbn="978-0132-35-088-4", genre="programare calculatoare",
#                              publication_date="2008-08-01",
#                              pages=464,
#                              description="Noted software expert Robert C. Martin presents a revolutionary paradigm with 'Clean Code: A Handbook of Agile Software Craftsmanship'. Martin has teamed up with his colleagues from Object Mentor to distill their best agile practice of cleaning code 'on the fly' into a book that will instill within you the values of a software craftsman and make you a better programmer-but only if you work at it."
#                                          "'Clean Code' is divided into three parts. The first describes the principles, patterns, and practices of writing clean code. The second part consists of several case studies of increasing complexity. Each case study is an exercise in cleaning up code-of transforming a code base that has some problems into one that is sound and efficient. The third part is the payoff: a single chapter containing a list of heuristics and 'smells' gathered while creating the case studies."
#                                          "The result is a knowledge base that describes the way we think when we write, read, and clean code.",
#                              price=245.00, stock=0, category=category4, promotion=promotion3,
#                              available_after_stock_empty=True)
# author26 = Author.objects.create(name="Robert C. Martin", origin_author="Statele Unite")
# authorbook38 = AuthorBook.objects.create(book=book36, author=author26)
# publishers = Publisher.objects.filter(name="Editura Pearson Education(US)")
# publisher19 = publishers[0]
# publisherbook36 = PublisherBook.objects.create(book=book36, publisher=publisher19)
#
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book37 = Book.objects.create(title="The Clean Coder-A Code of Conduct for Professional Programmers",
#                              isbn="978-0137-08-107-3", genre="programare calculatoare",
#                              publication_date="2011-05-13",
#                              pages=256,
#                              description="Programmers who endure and succeed amidst swirling uncertainty and nonstop pressure share a common attribute: They care deeply about the practice of creating software. They treat it as a craft. They are professionals."
#                                          "In 'The Clean Coder: A Code of Conduct for Professional Programmers', legendary software expert Robert C. Martin introduces the disciplines, techniques, tools, and practices of true software craftsmanship. This book is packed with practical advice–about everything from estimating and coding to refactoring and testing."
#                                          "It covers much more than technique: It is about attitude. Martin shows how to approach software development with honor, self-respect, and pride; work well and work clean; communicate and estimate faithfully; face difficult decisions with clarity and honesty; and understand that deep knowledge comes with a responsibility to act.",
#                              price=213.00, stock=0, category=category4, promotion=promotion3,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Robert C. Martin")
# author26 = authors[0]
# authorbook39 = AuthorBook.objects.create(book=book37, author=author26)
# publishers = Publisher.objects.filter(name="Editura Pearson Education(US)")
# publisher19 = publishers[0]
# publisherbook37 = PublisherBook.objects.create(book=book37, publisher=publisher19)
#
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book38 = Book.objects.create(title="Learn Ruby the Hard Way",
#                              isbn="978-0321-88-499-2", genre="programare calculatoare",
#                              publication_date="2014-12-17",
#                              pages=336,
#                              description="You Will Learn Ruby!Zed Shaw has perfected the world's best system for learning Ruby. Follow it and you will succeed-just like the hundreds of thousands of beginners Zed has taught to date! You bring the discipline, commitment, and persistence; the author supplies everything else."
#                                          "In Learn Ruby the Hard Way, Third Edition, you'll learn Ruby by working through 52 brilliantly crafted exercises. Read them. Type their code precisely. (No copying and pasting!) Fix your mistakes. Watch the programs run."
#                                          "As you do, you'll learn how software works; what good programs look like; how to read, write, and think about code; and how to find and fix your mistakes using tricks professional programmers use.",
#                              price=159.00, stock=0, category=category4, promotion=promotion3,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Zed A. Shaw")
# author25 = authors[0]
# authorbook40 = AuthorBook.objects.create(book=book38, author=author25)
# publishers = Publisher.objects.filter(name="Editura Pearson Education(US)")
# publisher19 = publishers[0]
# publisherbook38 = PublisherBook.objects.create(book=book38, publisher=publisher19)
#
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book39 = Book.objects.create(
#     title="Head First Design Patterns: Building Extensible and Maintainable Object-Oriented Software",
#     isbn="978-1492-07-800-5", genre="programare calculatoare",
#     publication_date="2021-01-05",
#     pages=694,
#     description="What will you learn from this book?You know you don't want to reinvent the wheel, so you look to Design Patterns: the lessons learned by those who've faced the same software design problems."
#                 "With Design Patterns, you get to take advantage of the best practices and experience of others so you can spend your time on something more challenging. Something more fun."
#                 "This book shows you the patterns that matter, when to use them and why, how to apply them to your own designs, and the object-oriented design principles on which they're based. Join hundreds of thousands of developers who've improved their object-oriented design skills through Head First Design Patterns."
#                 "If you've read a Head First book, you know what to expect: a visually rich format designed for the way your brain works. With Head First Design Patterns, 2E you'll learn design principles and patterns in a way that won't put you to sleep, so you can get out there to solve software design problems and speak the language of patterns with others on your team.",
#     price=314.00, stock=0, category=category4, promotion=promotion3,
#     available_after_stock_empty=True)
# author27 = Author.objects.create(name="Elisabeth Robson", origin_author="Statele Unite")
# author28 = Author.objects.create(name="Eric Freeman", origin_author="Statele Unite")
# authorbook41 = AuthorBook.objects.create(book=book39, author=author27)
# authorbook42 = AuthorBook.objects.create(book=book39, author=author28)
# publishers = Publisher.objects.filter(name="Editura  O'Reilly Media, Inc")
# publisher17 = publishers[0]
# publisherbook39 = PublisherBook.objects.create(book=book39, publisher=publisher17)
#
# categorys = Category.objects.filter(name="CALCULATOARE/IT")
# category4 = categorys[0]
# promotions = Promotion.objects.filter(name="Reduceri finale de sezon")
# promotion3 = promotions[0]
# book40 = Book.objects.create(title="Head First Design Patterns: A Brain-Friendly Guide",
#                              isbn="978-0596-00-712-6", genre="programare calculatoare",
#                              publication_date="2004-10-01",
#                              pages=608,
#                              description="What’s so special about design patterns?At any given moment, someone struggles with the same software design problems you have. And, chances are, someone else has already solved your problem."
#                                          "This edition of 'Head First Design Patterns'—now updated for Java 8—shows you the tried-and-true, road-tested patterns used by developers to create functional, elegant, reusable, and flexible software."
#                                          "By the time you finish this book, you’ll be able to take advantage of the best design practices and experiences of those who have fought the beast of software design and triumphed."
#                                          "What’s so special about this book?We think your time is too valuable to spend struggling with new concepts."
#                                          "Using the latest research in cognitive science and learning theory to craft a multi-sensory learning experience, 'Head First Design Patterns' uses a visually rich format designed for the way your brain works, not a text-heavy approach that puts you to sleep.",
#                              price=364.00, stock=0, category=category4, promotion=promotion3,
#                              available_after_stock_empty=True)
# authors = Author.objects.filter(name="Elisabeth Robson")
# author27 = authors[0]
# authors = Author.objects.filter(name="Eric Freeman")
# author28 = authors[0]
# author29 = Author.objects.create(name="Bert Bates", origin_author="Statele Unite")
# author30 = Author.objects.create(name="Kathy Sierra", origin_author="Statele Unite")
#
# authors = Author.objects.filter(name="Elisabeth Robson")
# author27 = authors[0]
# authors = Author.objects.filter(name="Eric Freeman")
# author28 = authors[0]
# authors = Author.objects.filter(name="Bert Bates")
# author29 = authors[0]
# authors = Author.objects.filter(name="Kathy Sierra")
# author30 = authors[0]
# books = Book.objects.filter(title="Head First Design Patterns: A Brain-Friendly Guide")
# book40 = books[0]
# authorbook43 = AuthorBook.objects.create(book=book40, author=author27)
# authorbook44 = AuthorBook.objects.create(book=book40, author=author28)
# authorbook45 = AuthorBook.objects.create(book=book40, author=author29)
# authorbook46 = AuthorBook.objects.create(book=book40, author=author30)
# publishers = Publisher.objects.filter(name="Editura  O'Reilly Media, Inc")
# publisher17 = publishers[0]
# publisherbook40 = PublisherBook.objects.create(book=book40, publisher=publisher17)